import os

from Grid import Grid
from TimeDomain import TimeDomain
from Pump import Pump
from ObservationPoint import ObservationPoint

class Model:
    #expects (Grid, TimeDomain, List of Pumps, List of ObservationPoints).
    #if you don't want to have any pumps, just give an empty list, e.g. 'list()'
    #to give a permeability field, put an .h5-file named permeability_data.h5 into the folder of all the python files
    def __init__(self, grid, time_domain, pumps, observations):
        self.grid = grid
        self.time = time_domain
        self.pumps = pumps
        self.observations = observations
        if(os.path.isfile("permeability_data.h5")): #TODO adapt file name
            self.permeability = True
        else:
            self.permeability = False