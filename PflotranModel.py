# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

import os

from FlowModel import Model

class FluidSolver:
    def __init__(self, model):
        self.model = model

    def create_input_file(self):
        current_path= os.getcwd()

        with open('pflotran.in', 'w') as pfile:

            simulation_block = "SIMULATION\n"\
                             "\tSIMULATION_TYPE SUBSURFACE\n"\
                             "\tPROCESS_MODELS\n"\
                             "\t\tSUBSURFACE_FLOW flow\n"\
                             "\t\t\tMODE TH\n"\
                             "\t\t/\n"\
                             "\t/\n"\
                             "END\n"

            th_block = "SUBSURFACE\n" \
                       "NUMERICAL_METHODS FLOW\n" \
                       "\tNEWTON_SOLVER\n" \
                       "\t\tANALYTICAL_JACOBIAN\n" \
                       "\t\tITOL_UPDATE 1.d0\n" \
                       "\t\tRTOL 1.d-3\n" \
                       "\t/\n" \
                       "\tLINEAR_SOLVER\n" \
                       "\t\tSOLVER DIRECT\n" \
                       "\t/\n" \
                       "END\n"

            grid_block = self.model.grid.to_pflotran()

            fluid_prop_block = "FLUID_PROPERTY\n" \
                               "\tDIFFUSION_COEFFICIENT 1.d-9\n" \
                               "/\n"
            if(self.model.permeability):
                #TODO: change file name
                permeability_dataset =  "DATASET permeability_values\n" \
                                        "\tFILENAME permeability_values.h5\n" \
                                        "\tHDF5_DATASET_NAME permeability_values\n" \
                                        "END\n"
            else:
                permeability_dataset = ""
            material_prop_block = "MATERIAL_PROPERTY gravel\n" \
                                  "\tID 1\n" \
                                  "\tPOROSITY 0.25d0\n" \
                                  "\tTORTUOSITY 0.5d0\n" \
                                  "\tROCK_DENSITY 2.8E3\n" \
                                  "\tSPECIFIC_HEAT 1E3\n" \
                                  "\tTHERMAL_CONDUCTIVITY_DRY 0.5\n" \
                                  "\tTHERMAL_CONDUCTIVITY_WET 0.5\n" \
                                  "\tLONGITUDINAL_DISPERSIVITY 3.1536D0\n" \
                                  "\tPERMEABILITY\n"
                            
            material_prop_block += "\t\tDATASET permeability_values\n" if self.model.permeability else "\t\tPERM_ISO 1.d-10\n"
            material_prop_block += "\t/\n" \
                                  "\tCHARACTERISTIC_CURVES cc1\n" \
                                  "/\n"

            characteristic_curves_block = "CHARACTERISTIC_CURVES cc1\n" \
                                          "\tSATURATION_FUNCTION VAN_GENUCHTEN\n" \
                                          "\t\tALPHA 1.d-4\n" \
                                          "\t\tM 0.5d0\n" \
                                          "\t\tLIQUID_RESIDUAL_SATURATION 0.1d0\n" \
                                          "\t/\n" \
                                          "\t\tPERMEABILITY_FUNCTION MUALEM_VG_LIQ\n" \
                                          "\t\tM 0.5d0\n" \
                                          "\t\tLIQUID_RESIDUAL_SATURATION 0.1d0\n" \
                                          "\t/\n" \
                                          "END\n"

            observations_block = ""
            for obs in self.model.observations:
                observations_block += obs.to_pflotran()
            #CHANGE DATUM HERE!!
            init_flow_block = "FLOW_CONDITION initial\n" \
                              "\tTYPE\n" \
                              "\t\tPRESSURE HYDROSTATIC\n" \
                              "\t\tTEMPERATURE DIRICHLET\n" \
                              "\t/\n" \
                              "\tDATUM 0.d0 0.d0 10.d0\n" \
                              "\tGRADIENT\n" \
                              "\t\tPRESSURE -0.01 0. 0.\n" \
                              "\t/\n" \
                              "\tPRESSURE 101325.d0\n" \
                              "\tTEMPERATURE 10.d0\n" \
                              "END\n"


            #assumption periodic time == max time step
            max_time_step = self.model.time.max_step
            output_block = "OUTPUT\n" \
                            "\tSNAPSHOT_FILE\n" \
                                                                "\t\tPERIODIC TIME " + str(max_time_step) + "d0 d\n" \
                                                                "\t\tFORMAT VTK\n" \
                                                                "\t\tPRINT_COLUMN_IDS\n" \
                                                                "\t\tVARIABLES\n" \
                                                                "\t\t\tLIQUID_PRESSURE\n" \
                                                                "\t\t\tTEMPERATURE\n" \
                                                                "\t\t/\n" \
                                                                "\t\tVELOCITY_AT_CENTER\n" \
                                                                "\t/\n" \
                                                                "\tOBSERVATION_FILE\n" \
                                                                "\t\tPERIODIC TIME " + str(max_time_step) + " d\n" \
                                                                                                       "\t\tVARIABLES\n" \
                                                                                                       "\t\t\tTEMPERATURE\n" \
                                                                                                       "\t\t\tLIQUID_PRESSURE\n" \
                                                                                                       "\t\t/\n" \
                                                                                                       "\t/\n" \
                                                                                                       "END\n"

            strati_block = "STRATA\n" \
                           "\tREGION All\n" \
                           "\tMATERIAL gravel\n" \
                           "END\n"

            end_subsurface_block = "HDF5_READ_GROUP_SIZE 1\n" \
                                   "END_SUBSURFACE\n"

            condition_coupler_block_init = "INITIAL_CONDITION all\n" \
                                            "\tFLOW_CONDITION initial\n" \
                                            "\tREGION All\n" \
                                            "END\n"

            condition_coupler_block_west = "BOUNDARY_CONDITION inflow\n" \
                                           "\tFLOW_CONDITION initial\n" \
                                           "\tREGION west\n" \
                                           "END\n"

            condition_coupler_block_west = "BOUNDARY_CONDITION inflow\n" \
                                           "\tFLOW_CONDITION initial\n" \
                                           "\tREGION east\n" \
                                           "END\n"

            time_block = "TIME\n" \
                         "\tFINAL_TIME " + str(self.model.time.days) + " d\n" \
                         "\tMAXIMUM_TIMESTEP_SIZE " + str(self.model.time.max_step) + "d0 d\n" \
                         "END\n"
            
            region_block = "REGION All\n" \
                           "\tCOORDINATES\n" \
                           "\t\t0.d0 0.d0 0.d0\n" \
                           "\t\t" + self.model.grid.bound_pos.to_pflotran()+"\n" \
                           "\t/\n" \
                           "END\n\n" \
                           "REGION top\n" \
                           "\tCOORDINATES\n" \
                           "\t\t0.d0 0.d0 " + str(self.model.grid.bound_pos.z) + ".d0\n" \
                           "\t\t" + self.model.grid.bound_pos.to_pflotran()+"\n" \
                           "\t/\n" \
                           "\tFACE TOP\n" \
                           "END\n\n" \
                           "REGION bottom\n" \
                           "\tCOORDINATES\n" \
                           "\t\t0.d0 0.d0 0.d0\n" \
                           "\t\t" + str(self.model.grid.bound_pos.x) + ".d0 " + str(self.model.grid.bound_pos.y) + ".d0 0.d0\n" \
                           "\t/\n" \
                           "\tFACE BOTTOM\n" \
                           "END\n\n" \
                           "REGION west\n" \
                           "\tCOORDINATES\n" \
                           "\t\t0.d0 0.d0 0.d0\n" \
                           "\t\t0.d0 " + str(self.model.grid.bound_pos.y) + ".d0 " + str(self.model.grid.bound_pos.z) + ".d0\n" \
                           "\t/\n" \
                           "\tFACE WEST\n" \
                           "END\n\n" \
                           "REGION east\n" \
                           "\tCOORDINATES\n" \
                           "\t\t" + str(self.model.grid.bound_pos.x) + ".d0 0.d0 0.d0\n" \
                           "\t\t" + self.model.grid.bound_pos.to_pflotran()+"\n" \
                           "\t/\n" \
                           "\tFACE EAST\n" \
                           "END\n\n" \
                           "REGION north\n" \
                           "\tCOORDINATES\n" \
                           "\t\t0.d0 " + str(self.model.grid.bound_pos.y) + ".d0 0.d0\n" \
                           "\t\t" + self.model.grid.bound_pos.to_pflotran()+"\n" \
                           "\t/\n" \
                           "\tFACE NORTH\n" \
                           "END\n\n" \
                           "REGION south\n" \
                           "\tCOORDINATES\n" \
                           "\t\t0.d0 0.d0 0.d0\n" \
                           "\t\t" + str(self.model.grid.bound_pos.x) + ".d0 0.d0 " + str(self.model.grid.bound_pos.z) + ".d0\n" \
                           "\t/\n" \
                           "\tFACE SOUTH\n" \
                           "END\n\n"
            pump_block = ""
            for pump in self.model.pumps:
                pump_block += pump.to_pflotran()

            input_string = simulation_block + th_block + grid_block + fluid_prop_block + material_prop_block + characteristic_curves_block + output_block + permeability_dataset + time_block + region_block + pump_block + init_flow_block + observations_block + condition_coupler_block_init + condition_coupler_block_west + strati_block + end_subsurface_block
            pfile.write(input_string)
