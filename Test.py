from PflotranModel import FluidSolver
from Grid import Grid
from Position import Position
from TimeDomain import TimeDomain
from Pump import Pump
from ObservationPoint import ObservationPoint
from FlowModel import Model

#Test 1
grid_end_point = Position(10,10,10)
grid = Grid(10,10,10,grid_end_point)
time_domain = TimeDomain(10,0.1)
pumps = list()
timesteps = list()
for i in range(8):
    timesteps.append(str(i)+".d0")
mass_flows = list()
for i in range(8):
    mass_flows.append("1.d0")
temperatures = list()
for i in range(8):
    temperatures.append("15.0")
for i in range(2,8,2):
    pumps.append(Pump(Position(i,i,i), timesteps, mass_flows, temperatures, 1))
observations = list()
for i in range(1):
    observations.append(ObservationPoint(Position(5,5,5)))
model = Model(grid, time_domain, pumps, observations)
solver = FluidSolver(model)

solver.create_input_file()