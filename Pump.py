from enum import Enum

from Position import Position

static_pump_counter = 1

class PumpState(Enum):
    ON = True
    OFF = False

class Pump:
    #note that pumps are numbered after their time of creation (e.g. the first pump's flow condition is called 'fc_pump_1' and its region is called 'region_pump_1')
    def __init__(self, position, timesteps, mass_flows, temperatures, state):
        global static_pump_counter
        self.id = static_pump_counter
        static_pump_counter += 1
        self.position = position
        self.timesteps = timesteps
        self.mass_flows = mass_flows
        self.temperatures = temperatures
        self.state = state

        self.flow_condition_name = f"fc_pump_{self.id}"
        self.region_name = f"region_pump_{self.id}"

        

    def tabify_list(self,lst, num_tabs=1):
        string =""
        for time_step, data in zip(self.timesteps,lst):
            for i in range(num_tabs):
                string+="\t"
            string = string + f"{time_step} {data}\n"
        return string

    def create_source_sink(self):
        return f"""
SOURCE_SINK
    FLOW_CONDITION {self.flow_condition_name}
    REGION {self.region_name}
END
        """
    
    def create_region(self):
        return f"""
REGION {self.region_name}
    COORDINATE {self.position.to_pflotran()}
END
        """

    def create_flow_condition(self):
        return f""" 
FLOW_CONDITION {self.flow_condition_name}
    TYPE
        RATE SCALED_MASS_RATE VOLUME
        TEMPERATURE DIRICHLET
    /

    RATE LIST
        TIME_UNITS d
        DATA_UNITS kg/s
        INTERPOLATION LINEAR
        #time   #massrate
{self.tabify_list(self.mass_flows, num_tabs=2)}
    /

    TEMPERATURE LIST
        TIME_UNITS d
        DATA_UNITS C
        INTERPOLATION LINEAR
        #time   #temperature
{self.tabify_list(self.temperatures, num_tabs=2)}
    /
END
        """

    def to_pflotran(self):
        return f"""
        {self.create_region()}
        {self.create_flow_condition()}
        {self.create_source_sink()}
        """

