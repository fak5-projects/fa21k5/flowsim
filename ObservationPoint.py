from enum import Enum

from Position import Position

static_observation_counter = 1

class Quantity(Enum):
    Temperature = 1
    LiquidPressure = 2
    Permeability = 3

class ObservationPoint:
    #orders PFLOTRAN to put data observed at the specified coordinate into the OBSERVATION_FILE
    def __init__(self, position):
        global static_observation_counter
        self.pos = position
        self.pflotran_string = "REGION observation_"+str(static_observation_counter)+"\n\tCOORDINATE "+self.pos.to_pflotran()+"\n/\nOBSERVATION\n\tREGION observation_"+str(static_observation_counter)+"\n\tAT_COORDINATE " + self.pos.to_pflotran() + "\n/\n"
        static_observation_counter += 1

    def to_pflotran(self):
        return self.pflotran_string